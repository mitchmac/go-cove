package main

import(
    "io"
    "io/ioutil"
    "path/filepath"
    "net/http"
    "encoding/xml"
    "github.com/gorilla/mux"
)

func main() {
    r := mux.NewRouter()
    r.HandleFunc("/", homePage)
    r.HandleFunc("/cron", cron)
    http.Handle("/", r)
    http.ListenAndServe(":8081", nil)
}

func homePage(w http.ResponseWriter, req *http.Request) {
    io.WriteString(w, "hello, world!\n")
}

func cron(w http.ResponseWriter, req *http.Request) {
  contentFiles, err := filepath.Glob("content/*.xml")
	if err == nil {
		for i := range contentFiles {
			b, err := ioutil.ReadFile(contentFiles[i])
			if err == nil {
				Post := Post{}
				err := xml.Unmarshal(b, &Post)
				if err != nil {
					panic(err)
				}
				io.WriteString(w, Post.Path)
			}
		}
	}
}

type Post struct {
	XMLName xml.Name `xml:"post"`
	Path  string `xml:"path"`
}
